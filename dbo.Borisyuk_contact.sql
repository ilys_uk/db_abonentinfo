﻿CREATE TABLE [dbo].[Borisyuk_contact] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [phone]       NVARCHAR (11) NOT NULL,
    [type]        NVARCHAR (20) NOT NULL,
    [provider_id] INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Borisyuk_contact_provider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[Borisyuk_provider] ([Id])
);

