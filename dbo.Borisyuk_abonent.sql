﻿CREATE TABLE [dbo].[Borisyuk_abonent] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [name]       NVARCHAR (50) NOT NULL,
    [surname]    NVARCHAR (50) NOT NULL,
    [patronymic] NVARCHAR (50) NOT NULL,
    [adress]     TEXT          NOT NULL,
    [birthdate]  DATETIME      NULL,
    [comment]    TEXT          NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

