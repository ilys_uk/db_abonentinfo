﻿CREATE TABLE [dbo].[Borisyuk_abonent_has_contact] (
    [abonent_id] INT NOT NULL,
    [contact_id] INT NOT NULL,
    CONSTRAINT [FK_Borisyuk_abonent_has_contact_abonent] FOREIGN KEY ([abonent_id]) REFERENCES [dbo].[Borisyuk_abonent] ([Id]),
    CONSTRAINT [FK_Borisyuk_abonent_has_contact_contact] FOREIGN KEY ([contact_id]) REFERENCES [dbo].[Borisyuk_contact] ([Id])
);

