﻿namespace AbonentInfo
{
    partial class ProvForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ProvForm_Score_TextBox = new System.Windows.Forms.TextBox();
            this.AbFom_CANCEL_Button = new System.Windows.Forms.Button();
            this.ProvForm_Name_TextBox = new System.Windows.Forms.TextBox();
            this.AbFom_OK_Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.ProvForm_Score_TextBox);
            this.groupBox1.Controls.Add(this.AbFom_CANCEL_Button);
            this.groupBox1.Controls.Add(this.ProvForm_Name_TextBox);
            this.groupBox1.Controls.Add(this.AbFom_OK_Button);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(13, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(340, 161);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Провайдер";
            // 
            // ProvForm_Score_TextBox
            // 
            this.ProvForm_Score_TextBox.Location = new System.Drawing.Point(116, 67);
            this.ProvForm_Score_TextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProvForm_Score_TextBox.Name = "ProvForm_Score_TextBox";
            this.ProvForm_Score_TextBox.Size = new System.Drawing.Size(209, 23);
            this.ProvForm_Score_TextBox.TabIndex = 20;
            // 
            // AbFom_CANCEL_Button
            // 
            this.AbFom_CANCEL_Button.Location = new System.Drawing.Point(226, 111);
            this.AbFom_CANCEL_Button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AbFom_CANCEL_Button.Name = "AbFom_CANCEL_Button";
            this.AbFom_CANCEL_Button.Size = new System.Drawing.Size(99, 30);
            this.AbFom_CANCEL_Button.TabIndex = 15;
            this.AbFom_CANCEL_Button.Text = "Выход";
            this.AbFom_CANCEL_Button.UseVisualStyleBackColor = true;
            this.AbFom_CANCEL_Button.Click += new System.EventHandler(this.AbFom_CANCEL_Button_Click);
            // 
            // ProvForm_Name_TextBox
            // 
            this.ProvForm_Name_TextBox.Location = new System.Drawing.Point(116, 30);
            this.ProvForm_Name_TextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProvForm_Name_TextBox.Name = "ProvForm_Name_TextBox";
            this.ProvForm_Name_TextBox.Size = new System.Drawing.Size(209, 23);
            this.ProvForm_Name_TextBox.TabIndex = 19;
            // 
            // AbFom_OK_Button
            // 
            this.AbFom_OK_Button.Location = new System.Drawing.Point(23, 111);
            this.AbFom_OK_Button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AbFom_OK_Button.Name = "AbFom_OK_Button";
            this.AbFom_OK_Button.Size = new System.Drawing.Size(185, 30);
            this.AbFom_OK_Button.TabIndex = 14;
            this.AbFom_OK_Button.Text = "Добавить";
            this.AbFom_OK_Button.UseVisualStyleBackColor = true;
            this.AbFom_OK_Button.Click += new System.EventHandler(this.AbFom_OK_Button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 18;
            this.label2.Text = "Рейтинг";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Название";
            // 
            // ProvForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 188);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ProvForm";
            this.Text = "Добавление провайдера";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AbFom_CANCEL_Button;
        private System.Windows.Forms.Button AbFom_OK_Button;
        public System.Windows.Forms.TextBox ProvForm_Score_TextBox;
        public System.Windows.Forms.TextBox ProvForm_Name_TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}