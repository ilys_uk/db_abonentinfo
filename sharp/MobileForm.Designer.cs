﻿namespace AbonentInfo
{
    partial class ConForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ConForm_ComBox_Prov = new System.Windows.Forms.ComboBox();
            this.AbFom_CANCEL_Button = new System.Windows.Forms.Button();
            this.AbFom_OK_Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ConForm_Type_TextBox = new System.Windows.Forms.TextBox();
            this.ConForm_Phone_Number_TextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ConForm_ComBox_Prov);
            this.groupBox1.Controls.Add(this.AbFom_CANCEL_Button);
            this.groupBox1.Controls.Add(this.AbFom_OK_Button);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ConForm_Type_TextBox);
            this.groupBox1.Controls.Add(this.ConForm_Phone_Number_TextBox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(435, 177);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Контакт";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 101);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Провайдер";
            // 
            // ConForm_ComBox_Prov
            // 
            this.ConForm_ComBox_Prov.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ConForm_ComBox_Prov.FormattingEnabled = true;
            this.ConForm_ComBox_Prov.Location = new System.Drawing.Point(201, 97);
            this.ConForm_ComBox_Prov.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ConForm_ComBox_Prov.Name = "ConForm_ComBox_Prov";
            this.ConForm_ComBox_Prov.Size = new System.Drawing.Size(200, 25);
            this.ConForm_ComBox_Prov.TabIndex = 16;
            // 
            // AbFom_CANCEL_Button
            // 
            this.AbFom_CANCEL_Button.Location = new System.Drawing.Point(305, 138);
            this.AbFom_CANCEL_Button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AbFom_CANCEL_Button.Name = "AbFom_CANCEL_Button";
            this.AbFom_CANCEL_Button.Size = new System.Drawing.Size(99, 30);
            this.AbFom_CANCEL_Button.TabIndex = 15;
            this.AbFom_CANCEL_Button.Text = "Отмена";
            this.AbFom_CANCEL_Button.UseVisualStyleBackColor = true;
            this.AbFom_CANCEL_Button.Click += new System.EventHandler(this.AbFom_CANCEL_Button_Click);
            // 
            // AbFom_OK_Button
            // 
            this.AbFom_OK_Button.Location = new System.Drawing.Point(32, 138);
            this.AbFom_OK_Button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.AbFom_OK_Button.Name = "AbFom_OK_Button";
            this.AbFom_OK_Button.Size = new System.Drawing.Size(265, 30);
            this.AbFom_OK_Button.TabIndex = 14;
            this.AbFom_OK_Button.Text = "Добавить";
            this.AbFom_OK_Button.UseVisualStyleBackColor = true;
            this.AbFom_OK_Button.Click += new System.EventHandler(this.AbFom_OK_Button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Тип телефона";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Номер телефона";
            // 
            // ConForm_Type_TextBox
            // 
            this.ConForm_Type_TextBox.Location = new System.Drawing.Point(201, 65);
            this.ConForm_Type_TextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ConForm_Type_TextBox.Name = "ConForm_Type_TextBox";
            this.ConForm_Type_TextBox.Size = new System.Drawing.Size(201, 23);
            this.ConForm_Type_TextBox.TabIndex = 2;
            // 
            // ConForm_Phone_Number_TextBox
            // 
            this.ConForm_Phone_Number_TextBox.Location = new System.Drawing.Point(201, 33);
            this.ConForm_Phone_Number_TextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ConForm_Phone_Number_TextBox.Name = "ConForm_Phone_Number_TextBox";
            this.ConForm_Phone_Number_TextBox.Size = new System.Drawing.Size(201, 23);
            this.ConForm_Phone_Number_TextBox.TabIndex = 0;
            // 
            // ConForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 204);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ConForm";
            this.Text = "Номер телефона";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AbFom_CANCEL_Button;
        private System.Windows.Forms.Button AbFom_OK_Button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox ConForm_Type_TextBox;
        public System.Windows.Forms.TextBox ConForm_Phone_Number_TextBox;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox ConForm_ComBox_Prov;
    }
}