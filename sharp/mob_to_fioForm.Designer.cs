﻿namespace AbonentInfo
{
    partial class PhDirecForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FIO_PhDirecForm_ComBox = new System.Windows.Forms.ComboBox();
            this.Phone_PhDirecForm_ComBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PhDirecCancel_BUTTON = new System.Windows.Forms.Button();
            this.PhDirecOK_BUTTON = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FIO_PhDirecForm_ComBox
            // 
            this.FIO_PhDirecForm_ComBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FIO_PhDirecForm_ComBox.Location = new System.Drawing.Point(196, 37);
            this.FIO_PhDirecForm_ComBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FIO_PhDirecForm_ComBox.Name = "FIO_PhDirecForm_ComBox";
            this.FIO_PhDirecForm_ComBox.Size = new System.Drawing.Size(232, 24);
            this.FIO_PhDirecForm_ComBox.TabIndex = 0;
            // 
            // Phone_PhDirecForm_ComBox
            // 
            this.Phone_PhDirecForm_ComBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Phone_PhDirecForm_ComBox.FormattingEnabled = true;
            this.Phone_PhDirecForm_ComBox.Location = new System.Drawing.Point(196, 85);
            this.Phone_PhDirecForm_ComBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Phone_PhDirecForm_ComBox.Name = "Phone_PhDirecForm_ComBox";
            this.Phone_PhDirecForm_ComBox.Size = new System.Drawing.Size(229, 24);
            this.Phone_PhDirecForm_ComBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "ФИО";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 85);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Номер телефона";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.PhDirecCancel_BUTTON);
            this.groupBox1.Controls.Add(this.PhDirecOK_BUTTON);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Phone_PhDirecForm_ComBox);
            this.groupBox1.Controls.Add(this.FIO_PhDirecForm_ComBox);
            this.groupBox1.Location = new System.Drawing.Point(21, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(467, 181);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Тел.справочник";
            // 
            // PhDirecCancel_BUTTON
            // 
            this.PhDirecCancel_BUTTON.Location = new System.Drawing.Point(321, 133);
            this.PhDirecCancel_BUTTON.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PhDirecCancel_BUTTON.Name = "PhDirecCancel_BUTTON";
            this.PhDirecCancel_BUTTON.Size = new System.Drawing.Size(105, 33);
            this.PhDirecCancel_BUTTON.TabIndex = 7;
            this.PhDirecCancel_BUTTON.Text = "Выход";
            this.PhDirecCancel_BUTTON.UseVisualStyleBackColor = true;
            this.PhDirecCancel_BUTTON.Click += new System.EventHandler(this.PhDirecCancel_BUTTON_Click);
            // 
            // PhDirecOK_BUTTON
            // 
            this.PhDirecOK_BUTTON.Location = new System.Drawing.Point(30, 133);
            this.PhDirecOK_BUTTON.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PhDirecOK_BUTTON.Name = "PhDirecOK_BUTTON";
            this.PhDirecOK_BUTTON.Size = new System.Drawing.Size(270, 33);
            this.PhDirecOK_BUTTON.TabIndex = 6;
            this.PhDirecOK_BUTTON.Text = "Применить";
            this.PhDirecOK_BUTTON.UseVisualStyleBackColor = true;
            this.PhDirecOK_BUTTON.Click += new System.EventHandler(this.PhDirecOK_BUTTON_Click);
            // 
            // PhDirecForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 210);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PhDirecForm";
            this.Text = "Связь номера тлф с ФИО";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox FIO_PhDirecForm_ComBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button PhDirecCancel_BUTTON;
        private System.Windows.Forms.Button PhDirecOK_BUTTON;
        public System.Windows.Forms.ComboBox Phone_PhDirecForm_ComBox;
    }
}