﻿namespace AbonentInfo
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.contact = new System.Windows.Forms.TabControl();
            this.abonent_tabPage = new System.Windows.Forms.TabPage();
            this.DELETE_button = new System.Windows.Forms.Button();
            this.tabl_abonent = new System.Windows.Forms.DataGridView();
            this.CHANGE_button = new System.Windows.Forms.Button();
            this.CREATE_button = new System.Windows.Forms.Button();
            this.contact_tabPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.FindePhon = new System.Windows.Forms.TextBox();
            this.DELETE_CONTACT_BUTTON = new System.Windows.Forms.Button();
            this.CHANGE_CONTACT_BUTTON = new System.Windows.Forms.Button();
            this.CREATE_CONTACT_BUTTON = new System.Windows.Forms.Button();
            this.tabl_contact = new System.Windows.Forms.DataGridView();
            this.provider_tabPage = new System.Windows.Forms.TabPage();
            this.DELETE_PROV_BUTTON = new System.Windows.Forms.Button();
            this.CHANGE_PROV_BUTTON = new System.Windows.Forms.Button();
            this.CREATE_PROV_BUTTON = new System.Windows.Forms.Button();
            this.tabl_provayder = new System.Windows.Forms.DataGridView();
            this.info_tabPage = new System.Windows.Forms.TabPage();
            this.DELETE_PH_DICTIONARY = new System.Windows.Forms.Button();
            this.CHANGE_PH_DICTIONARY = new System.Windows.Forms.Button();
            this.CREATE_PH_DICTIONARY = new System.Windows.Forms.Button();
            this.tabl_info = new System.Windows.Forms.DataGridView();
            this.FIND_PHONE_BOX = new System.Windows.Forms.TextBox();
            this.FIND_FIO_BOX = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.contact.SuspendLayout();
            this.abonent_tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).BeginInit();
            this.contact_tabPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_contact)).BeginInit();
            this.provider_tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provayder)).BeginInit();
            this.info_tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_info)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contact
            // 
            this.contact.Controls.Add(this.abonent_tabPage);
            this.contact.Controls.Add(this.contact_tabPage);
            this.contact.Controls.Add(this.provider_tabPage);
            this.contact.Controls.Add(this.info_tabPage);
            this.contact.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.contact.Location = new System.Drawing.Point(2, 0);
            this.contact.Margin = new System.Windows.Forms.Padding(4);
            this.contact.Name = "contact";
            this.contact.SelectedIndex = 0;
            this.contact.Size = new System.Drawing.Size(924, 568);
            this.contact.TabIndex = 1;
            // 
            // abonent_tabPage
            // 
            this.abonent_tabPage.Controls.Add(this.DELETE_button);
            this.abonent_tabPage.Controls.Add(this.tabl_abonent);
            this.abonent_tabPage.Controls.Add(this.CREATE_button);
            this.abonent_tabPage.Controls.Add(this.CHANGE_button);
            this.abonent_tabPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.abonent_tabPage.Location = new System.Drawing.Point(4, 28);
            this.abonent_tabPage.Margin = new System.Windows.Forms.Padding(4);
            this.abonent_tabPage.Name = "abonent_tabPage";
            this.abonent_tabPage.Padding = new System.Windows.Forms.Padding(4);
            this.abonent_tabPage.Size = new System.Drawing.Size(916, 536);
            this.abonent_tabPage.TabIndex = 0;
            this.abonent_tabPage.Text = "Абонент";
            this.abonent_tabPage.UseVisualStyleBackColor = true;
            // 
            // DELETE_button
            // 
            this.DELETE_button.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DELETE_button.Location = new System.Drawing.Point(663, 470);
            this.DELETE_button.Margin = new System.Windows.Forms.Padding(4);
            this.DELETE_button.Name = "DELETE_button";
            this.DELETE_button.Size = new System.Drawing.Size(228, 56);
            this.DELETE_button.TabIndex = 8;
            this.DELETE_button.Text = "Удалить";
            this.DELETE_button.UseVisualStyleBackColor = true;
            this.DELETE_button.Click += new System.EventHandler(this.DELETE_button_Click);
            // 
            // tabl_abonent
            // 
            this.tabl_abonent.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            this.tabl_abonent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_abonent.Location = new System.Drawing.Point(4, 4);
            this.tabl_abonent.Margin = new System.Windows.Forms.Padding(4);
            this.tabl_abonent.Name = "tabl_abonent";
            this.tabl_abonent.RowHeadersWidth = 51;
            this.tabl_abonent.Size = new System.Drawing.Size(899, 458);
            this.tabl_abonent.TabIndex = 0;
            // 
            // CHANGE_button
            // 
            this.CHANGE_button.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CHANGE_button.Location = new System.Drawing.Point(351, 470);
            this.CHANGE_button.Margin = new System.Windows.Forms.Padding(4);
            this.CHANGE_button.Name = "CHANGE_button";
            this.CHANGE_button.Size = new System.Drawing.Size(256, 56);
            this.CHANGE_button.TabIndex = 7;
            this.CHANGE_button.Text = "Изменить";
            this.CHANGE_button.UseVisualStyleBackColor = true;
            this.CHANGE_button.Click += new System.EventHandler(this.CHANGE_button_Click);
            // 
            // CREATE_button
            // 
            this.CREATE_button.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.CREATE_button.Location = new System.Drawing.Point(22, 470);
            this.CREATE_button.Margin = new System.Windows.Forms.Padding(4);
            this.CREATE_button.Name = "CREATE_button";
            this.CREATE_button.Size = new System.Drawing.Size(301, 56);
            this.CREATE_button.TabIndex = 6;
            this.CREATE_button.Text = "Добавить абонента";
            this.CREATE_button.UseVisualStyleBackColor = true;
            this.CREATE_button.Click += new System.EventHandler(this.CREATE_button_Click);
            // 
            // contact_tabPage
            // 
            this.contact_tabPage.Controls.Add(this.groupBox2);
            this.contact_tabPage.Controls.Add(this.DELETE_CONTACT_BUTTON);
            this.contact_tabPage.Controls.Add(this.CHANGE_CONTACT_BUTTON);
            this.contact_tabPage.Controls.Add(this.CREATE_CONTACT_BUTTON);
            this.contact_tabPage.Controls.Add(this.tabl_contact);
            this.contact_tabPage.Location = new System.Drawing.Point(4, 28);
            this.contact_tabPage.Margin = new System.Windows.Forms.Padding(4);
            this.contact_tabPage.Name = "contact_tabPage";
            this.contact_tabPage.Padding = new System.Windows.Forms.Padding(4);
            this.contact_tabPage.Size = new System.Drawing.Size(916, 536);
            this.contact_tabPage.TabIndex = 1;
            this.contact_tabPage.Text = "Контакт";
            this.contact_tabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.FindePhon);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Underline);
            this.groupBox2.Location = new System.Drawing.Point(927, 203);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(262, 127);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск по:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label3.Location = new System.Drawing.Point(29, 81);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "ФИО";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label4.Location = new System.Drawing.Point(29, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Телефон";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBox1.Location = new System.Drawing.Point(118, 75);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(125, 24);
            this.textBox1.TabIndex = 3;
            // 
            // FindePhon
            // 
            this.FindePhon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.FindePhon.Location = new System.Drawing.Point(117, 23);
            this.FindePhon.Margin = new System.Windows.Forms.Padding(4);
            this.FindePhon.Name = "FindePhon";
            this.FindePhon.Size = new System.Drawing.Size(126, 24);
            this.FindePhon.TabIndex = 2;
            // 
            // DELETE_CONTACT_BUTTON
            // 
            this.DELETE_CONTACT_BUTTON.Location = new System.Drawing.Point(666, 471);
            this.DELETE_CONTACT_BUTTON.Margin = new System.Windows.Forms.Padding(4);
            this.DELETE_CONTACT_BUTTON.Name = "DELETE_CONTACT_BUTTON";
            this.DELETE_CONTACT_BUTTON.Size = new System.Drawing.Size(219, 57);
            this.DELETE_CONTACT_BUTTON.TabIndex = 11;
            this.DELETE_CONTACT_BUTTON.Text = "Удалить";
            this.DELETE_CONTACT_BUTTON.UseVisualStyleBackColor = true;
            this.DELETE_CONTACT_BUTTON.Click += new System.EventHandler(this.DELETE_CONTACT_BUTTON_Click);
            // 
            // CHANGE_CONTACT_BUTTON
            // 
            this.CHANGE_CONTACT_BUTTON.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.CHANGE_CONTACT_BUTTON.Location = new System.Drawing.Point(358, 468);
            this.CHANGE_CONTACT_BUTTON.Margin = new System.Windows.Forms.Padding(4);
            this.CHANGE_CONTACT_BUTTON.Name = "CHANGE_CONTACT_BUTTON";
            this.CHANGE_CONTACT_BUTTON.Size = new System.Drawing.Size(235, 60);
            this.CHANGE_CONTACT_BUTTON.TabIndex = 10;
            this.CHANGE_CONTACT_BUTTON.Text = "Изменить";
            this.CHANGE_CONTACT_BUTTON.UseVisualStyleBackColor = true;
            this.CHANGE_CONTACT_BUTTON.Click += new System.EventHandler(this.CHANGE_CONTACT_BUTTON_Click);
            // 
            // CREATE_CONTACT_BUTTON
            // 
            this.CREATE_CONTACT_BUTTON.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.CREATE_CONTACT_BUTTON.Location = new System.Drawing.Point(21, 468);
            this.CREATE_CONTACT_BUTTON.Margin = new System.Windows.Forms.Padding(4);
            this.CREATE_CONTACT_BUTTON.Name = "CREATE_CONTACT_BUTTON";
            this.CREATE_CONTACT_BUTTON.Size = new System.Drawing.Size(277, 60);
            this.CREATE_CONTACT_BUTTON.TabIndex = 9;
            this.CREATE_CONTACT_BUTTON.Text = "Добавить контакт";
            this.CREATE_CONTACT_BUTTON.UseVisualStyleBackColor = true;
            this.CREATE_CONTACT_BUTTON.Click += new System.EventHandler(this.CREATE_CONTACT_BUTTON_Click);
            // 
            // tabl_contact
            // 
            this.tabl_contact.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            this.tabl_contact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_contact.Location = new System.Drawing.Point(-4, 4);
            this.tabl_contact.Margin = new System.Windows.Forms.Padding(4);
            this.tabl_contact.Name = "tabl_contact";
            this.tabl_contact.RowHeadersWidth = 51;
            this.tabl_contact.Size = new System.Drawing.Size(912, 456);
            this.tabl_contact.TabIndex = 0;
            // 
            // provider_tabPage
            // 
            this.provider_tabPage.Controls.Add(this.DELETE_PROV_BUTTON);
            this.provider_tabPage.Controls.Add(this.CHANGE_PROV_BUTTON);
            this.provider_tabPage.Controls.Add(this.CREATE_PROV_BUTTON);
            this.provider_tabPage.Controls.Add(this.tabl_provayder);
            this.provider_tabPage.Location = new System.Drawing.Point(4, 28);
            this.provider_tabPage.Margin = new System.Windows.Forms.Padding(4);
            this.provider_tabPage.Name = "provider_tabPage";
            this.provider_tabPage.Padding = new System.Windows.Forms.Padding(4);
            this.provider_tabPage.Size = new System.Drawing.Size(916, 536);
            this.provider_tabPage.TabIndex = 2;
            this.provider_tabPage.Text = "Провайдеры";
            this.provider_tabPage.UseVisualStyleBackColor = true;
            // 
            // DELETE_PROV_BUTTON
            // 
            this.DELETE_PROV_BUTTON.Location = new System.Drawing.Point(656, 476);
            this.DELETE_PROV_BUTTON.Margin = new System.Windows.Forms.Padding(4);
            this.DELETE_PROV_BUTTON.Name = "DELETE_PROV_BUTTON";
            this.DELETE_PROV_BUTTON.Size = new System.Drawing.Size(233, 52);
            this.DELETE_PROV_BUTTON.TabIndex = 14;
            this.DELETE_PROV_BUTTON.Text = "Удалить";
            this.DELETE_PROV_BUTTON.UseVisualStyleBackColor = true;
            this.DELETE_PROV_BUTTON.Click += new System.EventHandler(this.DELETE_PROV_BUTTON_Click);
            // 
            // CHANGE_PROV_BUTTON
            // 
            this.CHANGE_PROV_BUTTON.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.CHANGE_PROV_BUTTON.Location = new System.Drawing.Point(355, 476);
            this.CHANGE_PROV_BUTTON.Margin = new System.Windows.Forms.Padding(4);
            this.CHANGE_PROV_BUTTON.Name = "CHANGE_PROV_BUTTON";
            this.CHANGE_PROV_BUTTON.Size = new System.Drawing.Size(235, 52);
            this.CHANGE_PROV_BUTTON.TabIndex = 13;
            this.CHANGE_PROV_BUTTON.Text = "Изменить";
            this.CHANGE_PROV_BUTTON.UseVisualStyleBackColor = true;
            this.CHANGE_PROV_BUTTON.Click += new System.EventHandler(this.CHANGE_PROV_BUTTON_Click);
            // 
            // CREATE_PROV_BUTTON
            // 
            this.CREATE_PROV_BUTTON.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.CREATE_PROV_BUTTON.Location = new System.Drawing.Point(37, 476);
            this.CREATE_PROV_BUTTON.Margin = new System.Windows.Forms.Padding(4);
            this.CREATE_PROV_BUTTON.Name = "CREATE_PROV_BUTTON";
            this.CREATE_PROV_BUTTON.Size = new System.Drawing.Size(235, 52);
            this.CREATE_PROV_BUTTON.TabIndex = 12;
            this.CREATE_PROV_BUTTON.Text = "Добавить провайдера";
            this.CREATE_PROV_BUTTON.UseVisualStyleBackColor = true;
            this.CREATE_PROV_BUTTON.Click += new System.EventHandler(this.CREATE_PROV_BUTTON_Click);
            // 
            // tabl_provayder
            // 
            this.tabl_provayder.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            this.tabl_provayder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_provayder.Location = new System.Drawing.Point(0, 4);
            this.tabl_provayder.Margin = new System.Windows.Forms.Padding(4);
            this.tabl_provayder.Name = "tabl_provayder";
            this.tabl_provayder.RowHeadersWidth = 51;
            this.tabl_provayder.Size = new System.Drawing.Size(908, 454);
            this.tabl_provayder.TabIndex = 0;
            // 
            // info_tabPage
            // 
            this.info_tabPage.Controls.Add(this.DELETE_PH_DICTIONARY);
            this.info_tabPage.Controls.Add(this.CHANGE_PH_DICTIONARY);
            this.info_tabPage.Controls.Add(this.CREATE_PH_DICTIONARY);
            this.info_tabPage.Controls.Add(this.tabl_info);
            this.info_tabPage.Location = new System.Drawing.Point(4, 28);
            this.info_tabPage.Margin = new System.Windows.Forms.Padding(4);
            this.info_tabPage.Name = "info_tabPage";
            this.info_tabPage.Padding = new System.Windows.Forms.Padding(4);
            this.info_tabPage.Size = new System.Drawing.Size(916, 536);
            this.info_tabPage.TabIndex = 3;
            this.info_tabPage.Text = "Тел.справ";
            this.info_tabPage.UseVisualStyleBackColor = true;
            // 
            // DELETE_PH_DICTIONARY
            // 
            this.DELETE_PH_DICTIONARY.Location = new System.Drawing.Point(670, 471);
            this.DELETE_PH_DICTIONARY.Margin = new System.Windows.Forms.Padding(4);
            this.DELETE_PH_DICTIONARY.Name = "DELETE_PH_DICTIONARY";
            this.DELETE_PH_DICTIONARY.Size = new System.Drawing.Size(199, 57);
            this.DELETE_PH_DICTIONARY.TabIndex = 3;
            this.DELETE_PH_DICTIONARY.Text = "Удалить";
            this.DELETE_PH_DICTIONARY.UseVisualStyleBackColor = true;
            this.DELETE_PH_DICTIONARY.Click += new System.EventHandler(this.DELETE_PH_DICTIONARY_Click);
            // 
            // CHANGE_PH_DICTIONARY
            // 
            this.CHANGE_PH_DICTIONARY.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.CHANGE_PH_DICTIONARY.Location = new System.Drawing.Point(349, 470);
            this.CHANGE_PH_DICTIONARY.Margin = new System.Windows.Forms.Padding(4);
            this.CHANGE_PH_DICTIONARY.Name = "CHANGE_PH_DICTIONARY";
            this.CHANGE_PH_DICTIONARY.Size = new System.Drawing.Size(241, 57);
            this.CHANGE_PH_DICTIONARY.TabIndex = 2;
            this.CHANGE_PH_DICTIONARY.Text = "Изменить";
            this.CHANGE_PH_DICTIONARY.UseVisualStyleBackColor = true;
            this.CHANGE_PH_DICTIONARY.Click += new System.EventHandler(this.CHANGE_PH_DICTIONARY_Click);
            // 
            // CREATE_PH_DICTIONARY
            // 
            this.CREATE_PH_DICTIONARY.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold);
            this.CREATE_PH_DICTIONARY.Location = new System.Drawing.Point(32, 471);
            this.CREATE_PH_DICTIONARY.Margin = new System.Windows.Forms.Padding(4);
            this.CREATE_PH_DICTIONARY.Name = "CREATE_PH_DICTIONARY";
            this.CREATE_PH_DICTIONARY.Size = new System.Drawing.Size(261, 57);
            this.CREATE_PH_DICTIONARY.TabIndex = 1;
            this.CREATE_PH_DICTIONARY.Text = "Добавить";
            this.CREATE_PH_DICTIONARY.UseVisualStyleBackColor = true;
            this.CREATE_PH_DICTIONARY.Click += new System.EventHandler(this.CREATE_PH_DICTIONARY_Click);
            // 
            // tabl_info
            // 
            this.tabl_info.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            this.tabl_info.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabl_info.Location = new System.Drawing.Point(4, 4);
            this.tabl_info.Margin = new System.Windows.Forms.Padding(4);
            this.tabl_info.Name = "tabl_info";
            this.tabl_info.RowHeadersWidth = 51;
            this.tabl_info.Size = new System.Drawing.Size(904, 459);
            this.tabl_info.TabIndex = 0;
            // 
            // FIND_PHONE_BOX
            // 
            this.FIND_PHONE_BOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.FIND_PHONE_BOX.Location = new System.Drawing.Point(342, 28);
            this.FIND_PHONE_BOX.Margin = new System.Windows.Forms.Padding(4);
            this.FIND_PHONE_BOX.Name = "FIND_PHONE_BOX";
            this.FIND_PHONE_BOX.Size = new System.Drawing.Size(145, 24);
            this.FIND_PHONE_BOX.TabIndex = 2;
            this.FIND_PHONE_BOX.TextChanged += new System.EventHandler(this.FIND_PHONE_BOX_TextChanged);
            // 
            // FIND_FIO_BOX
            // 
            this.FIND_FIO_BOX.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.FIND_FIO_BOX.Location = new System.Drawing.Point(74, 28);
            this.FIND_FIO_BOX.Margin = new System.Windows.Forms.Padding(4);
            this.FIND_FIO_BOX.Name = "FIND_FIO_BOX";
            this.FIND_FIO_BOX.Size = new System.Drawing.Size(146, 24);
            this.FIND_FIO_BOX.TabIndex = 3;
            this.FIND_FIO_BOX.TextChanged += new System.EventHandler(this.FIND_FIO_BOX_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label1.Location = new System.Drawing.Point(249, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Телефон";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label2.Location = new System.Drawing.Point(22, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "ФИО";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.FIND_PHONE_BOX);
            this.groupBox1.Controls.Add(this.FIND_FIO_BOX);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Underline);
            this.groupBox1.Location = new System.Drawing.Point(13, 576);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(499, 61);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск по:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 644);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.contact);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Телефонный справочник ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contact.ResumeLayout(false);
            this.abonent_tabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_abonent)).EndInit();
            this.contact_tabPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabl_contact)).EndInit();
            this.provider_tabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_provayder)).EndInit();
            this.info_tabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabl_info)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl contact;
        private System.Windows.Forms.TabPage abonent_tabPage;
        private System.Windows.Forms.TabPage contact_tabPage;
        private System.Windows.Forms.TabPage provider_tabPage;
        private System.Windows.Forms.TabPage info_tabPage;
        private System.Windows.Forms.DataGridView tabl_abonent;
        private System.Windows.Forms.DataGridView tabl_contact;
        private System.Windows.Forms.DataGridView tabl_provayder;
        private System.Windows.Forms.DataGridView tabl_info;
        private System.Windows.Forms.TextBox FIND_PHONE_BOX;
        private System.Windows.Forms.TextBox FIND_FIO_BOX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button CREATE_button;
        private System.Windows.Forms.Button CHANGE_button;
        private System.Windows.Forms.Button DELETE_button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button DELETE_CONTACT_BUTTON;
        private System.Windows.Forms.Button CHANGE_CONTACT_BUTTON;
        private System.Windows.Forms.Button CREATE_CONTACT_BUTTON;
        private System.Windows.Forms.Button DELETE_PROV_BUTTON;
        private System.Windows.Forms.Button CHANGE_PROV_BUTTON;
        private System.Windows.Forms.Button CREATE_PROV_BUTTON;
        private System.Windows.Forms.Button DELETE_PH_DICTIONARY;
        private System.Windows.Forms.Button CHANGE_PH_DICTIONARY;
        private System.Windows.Forms.Button CREATE_PH_DICTIONARY;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox FindePhon;
    }
}

