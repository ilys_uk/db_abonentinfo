﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AbonentInfo
{
    public partial class Form1 : Form
    {
        private string ConnectionString = global::AbonentInfo.Properties.Settings.Default.Database1ConnectionString;
        private const string AbonentTableName = "Borisyuk_abonent";
        private const string ContactTableName = "Borisyuk_contact";
       
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            updateAbonentDGW();
            updateContactDGW();
            updatePROVDGW();
            updatePHONEDGW();
        }

    

        void updateAbonentDGW()
        {
            var request = "SELECT * FROM [" + AbonentTableName + "] WHERE surname + ' ' + name + ' ' +patronymic LIKE '%" + FIND_FIO_BOX.Text + "%'";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var abonentTable = new DataTable();
            adapter.Fill(abonentTable);

            tabl_abonent.DataSource = abonentTable;

            tabl_abonent.Columns["id"].Visible = false;
            tabl_abonent.Columns["name"].HeaderText = "Имя";
            tabl_abonent.Columns["surname"].HeaderText = "Фамилия";
            tabl_abonent.Columns["patronymic"].HeaderText = "Отчество";
            tabl_abonent.Columns["birthdate"].HeaderText = "Дата Рождения";
            tabl_abonent.Columns["adress"].HeaderText = "Адрес";
            tabl_abonent.Columns["comment"].HeaderText = "Комментраий";

        }
        void updateContactDGW()
        {
            var request = "SELECT * FROM [" + ContactTableName + "] WHERE phone LIKE '%" + FIND_PHONE_BOX.Text + "%'";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var contactTable = new DataTable();
            adapter.Fill(contactTable);
            tabl_contact.DataSource = contactTable;
            tabl_contact.Columns["id"].Visible = false;
            tabl_contact.Columns["provider_id"].Visible = false;
            tabl_contact.Columns["phone"].HeaderText = "Телефон";
            tabl_contact.Columns["type"].HeaderText = "Тип";
        }

        void updatePROVDGW()
        {
            var request = "SELECT * FROM Borisyuk_provider";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var provTable = new DataTable();
            adapter.Fill(provTable);
            tabl_provayder.DataSource = provTable;
            tabl_provayder.Columns["id"].Visible = false;
            tabl_provayder.Columns["name"].HeaderText = "Имя";
            tabl_provayder.Columns["score"].HeaderText = "Рейтинг";
        }

        void updatePHONEDGW()
        {
            var request = @"SELECT *FROM Borisyuk_abonent JOIN  Borisyuk_abonent_has_contact ON Borisyuk_abonent.id= Borisyuk_abonent_has_contact.abonent_id 
            JOIN Borisyuk_contact ON Borisyuk_contact.id= Borisyuk_abonent_has_contact.contact_id LEFT JOIN Borisyuk_provider ON Borisyuk_provider.id=Borisyuk_contact.provider_id ";

            if (FIND_PHONE_BOX.Text != "")
            {
                request += @"WHERE  Borisyuk_contact.phone LIKE+'%" + FIND_PHONE_BOX.Text + "%'";

            }

            if (FIND_FIO_BOX.Text != "")
            {
                request += @" WHERE Borisyuk_abonent.surname+' '   +Borisyuk_abonent.patronymic+' ' +Borisyuk_abonent.name  LIKE+'%" + FIND_FIO_BOX.Text + "%'";
            }

            var adapter = new SqlDataAdapter(request, ConnectionString);
            var abonTable = new DataTable();
            adapter.Fill(abonTable);
            tabl_info.DataSource = abonTable;


            tabl_info.Columns["Id"].Visible = false;
            tabl_info.Columns["adress"].Visible = false;
            tabl_info.Columns["birthdate"].Visible = false;
            tabl_info.Columns["comment"].Visible = false;
            tabl_info.Columns["contact_id"].Visible = false;
            tabl_info.Columns["abonent_id"].Visible = false;
          
            tabl_info.Columns["type"].Visible = false;
            tabl_info.Columns["provider_id"].Visible = false;
            tabl_info.Columns["score"].Visible = false;
            tabl_info.Columns["name"].HeaderText = "Имя";
            tabl_info.Columns["surname"].HeaderText = "Фамилия";
            tabl_info.Columns["patronymic"].HeaderText = "Отчество";
            tabl_info.Columns["phone"].HeaderText = "Номер телефона";
            tabl_info.Columns["Id1"].Visible = false;
            tabl_info.Columns["Id2"].Visible = false;
            tabl_info.Columns["name1"].HeaderText = "Имя провайдера";
        }

        private void FIND_PHONE_BOX_TextChanged(object sender, EventArgs e)
        {
            updatePHONEDGW();
            updateContactDGW();
        }

        private void FIND_FIO_BOX_TextChanged(object sender, EventArgs e)
        {
            updatePHONEDGW();
            updateAbonentDGW();
        }

        //АБОНЕНТ
        private void CREATE_button_Click(object sender, EventArgs e)
        {

            var form = new AbForm();
            var res = form.ShowDialog();
            if(res==DialogResult.OK)
            {
               
                var surname = form.AbForm_SURNAME_TextBox.Text;
                var name = form.AbForm_NAME_TextBox.Text;
                var patronymic = form.AbForm_PATRONYMIC_TextBox.Text;
                var birthdate = Convert.ToDateTime(form.AbForm_BIRTHDATE_TextBox.Text).ToString("yyyy-MM-dd");
                var adress = form.AbForm_ADRESS_TextBox.Text;
                var comment = form.AbForm_COMMENT_TextBox.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Borisyuk_abonent (name, surname, patronymic, birthdate, adress, comment)" + "VALUES ('" + name + "', '" + surname + "', '"
                    + patronymic + "', '" + birthdate + "', '" + adress + "', '" + comment + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
                updateAbonentDGW();
            }
        }

        private void CHANGE_button_Click(object sender, EventArgs e)
        {
            var row = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new AbForm();
            form.AbForm_SURNAME_TextBox.Text = row.Cells["surname"].Value.ToString();
            form.AbForm_NAME_TextBox.Text = row.Cells["name"].Value.ToString();
            form.AbForm_PATRONYMIC_TextBox.Text = row.Cells["patronymic"].Value.ToString();
            form.AbForm_ADRESS_TextBox.Text = row.Cells["adress"].Value.ToString();
            form.AbForm_COMMENT_TextBox.Text= row.Cells["comment"].Value.ToString();
            form.AbForm_BIRTHDATE_TextBox.Text = row.Cells["birthdate"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var surname = form.AbForm_SURNAME_TextBox.Text;
                var name = form.AbForm_NAME_TextBox.Text;
                var patronymic = form.AbForm_PATRONYMIC_TextBox.Text;
                var comment = form.AbForm_COMMENT_TextBox.Text;
                var address = form.AbForm_ADRESS_TextBox.Text;
                //var birthday = form.AbForm_BIRTHDATE_TextBox.Text;
                var birthday = Convert.ToDateTime(form.AbForm_BIRTHDATE_TextBox.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
                var id = row.Cells["Id"].Value.ToString();

                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Borisyuk_abonent SET surname = '" + surname + "', name = '" + name + "', " +
                    "patronymic = '" + patronymic + "', adress = '" + address + "', comment = '" + comment + "', birthdate = '" + birthday + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateAbonentDGW();
            }
        }

        private void DELETE_button_Click(object sender, EventArgs e)
        {
            var row = tabl_abonent.SelectedRows.Count > 0 ? tabl_abonent.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Borisyuk_abonent WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateAbonentDGW();
        }

        //КОНТАКТ
        private void CREATE_CONTACT_BUTTON_Click(object sender, EventArgs e)
        {
            var form = new ConForm();
            {
                var getReq = "SELECT *FROM Borisyuk_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow row in providerTbl.Rows)
                {
                    dict.Add((int)row["Id"], row["name"].ToString());
                }
                form.ProviderData = dict;
            }
            
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number_TextBox.Text;
                var type = form.ConForm_Type_TextBox.Text;
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Borisyuk_contact (phone, type, provider_id) VALUES ('" + phone + "', '" + type + "', '" + provider_id.ToString() + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateContactDGW();
            }
        }

        private void CHANGE_CONTACT_BUTTON_Click(object sender, EventArgs e)
        {
            var row = tabl_contact.SelectedRows.Count > 0 ? tabl_contact.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new ConForm();
            form.ConForm_Phone_Number_TextBox.Text = row.Cells["phone"].Value.ToString();
            form.ConForm_Type_TextBox.Text = row.Cells["type"].Value.ToString();
            {
                var getReq = "SELECT *FROM Borisyuk_provider";
                var contactAdapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var providerTbl = new DataTable();
                contactAdapter.Fill(providerTbl);

                foreach (DataRow dbrow in providerTbl.Rows)
                {
                    dict.Add((int)dbrow["Id"], dbrow["name"].ToString());
                }
                form.ProviderData = dict;
            }
            form.ProviderID = (int)row.Cells["provider_id"].Value;
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var phone = form.ConForm_Phone_Number_TextBox.Text;
                var type = form.ConForm_Type_TextBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var provider_id = form.ProviderID;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Borisyuk_contact SET phone = '" + phone + "', type = '" + type + "', provider_id=" + provider_id.ToString()+
                     "WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updateContactDGW();
            }
        }

        private void DELETE_CONTACT_BUTTON_Click(object sender, EventArgs e)
        {
            var row = tabl_contact.SelectedRows.Count > 0 ? tabl_contact.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Borisyuk_contact WHERE Id = " + id + ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updateContactDGW();
        }

        //ПРОВАЙДЕР
        private void CREATE_PROV_BUTTON_Click(object sender, EventArgs e)
        {
            var form = new ProvForm();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.ProvForm_Name_TextBox.Text;
                var score = form.ProvForm_Score_TextBox.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Borisyuk_provider (name, score) VALUES ('" + name + "', '" + score + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updatePROVDGW();
            }

        }

        private void CHANGE_PROV_BUTTON_Click(object sender, EventArgs e)
        {
            var row = tabl_provayder.SelectedRows.Count > 0 ? tabl_provayder.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var form = new ProvForm();
            form.ProvForm_Name_TextBox.Text = row.Cells["name"].Value.ToString();
            form.ProvForm_Score_TextBox.Text = row.Cells["score"].Value.ToString();
            var res = form.ShowDialog();
            if (res == DialogResult.OK)
            {
                var name = form.ProvForm_Name_TextBox.Text;
                var score = form.ProvForm_Score_TextBox.Text;
                var id = row.Cells["Id"].Value.ToString();
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "UPDATE Borisyuk_provider SET name = '" + name + "', score = '" + score + "' WHERE Id = " + id + ";";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
                connection.Close();
                updatePROVDGW();
            }
        }

        private void DELETE_PROV_BUTTON_Click(object sender, EventArgs e)
        {

            
            var row = tabl_provayder.SelectedRows.Count > 0 ? tabl_provayder.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var id = row.Cells["Id"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Borisyuk_provider WHERE Id = " + id+ ";";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updatePROVDGW();
            updateContactDGW();
            updatePHONEDGW();
            updateAbonentDGW();
        }

        //ТЕЛЕФОННЫЙ СПРАВОЧНИК
        private void CREATE_PH_DICTIONARY_Click(object sender, EventArgs e)
        {
            var form = new PhDirecForm();

            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + AbonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }
                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + ContactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }
                form.ContactData = dict;
            }

            if (form.ShowDialog() == DialogResult.OK)
            {
                var conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "INSERT INTO Borisyuk_abonent_has_contact" + "(abonent_id, contact_id)" + " VALUES " + "('" + form.AbonentID + "', '" + form.ContactID + "')";
                var com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();

                updatePHONEDGW();
            }
        }

        private void CHANGE_PH_DICTIONARY_Click(object sender, EventArgs e)
        {

            var row2= tabl_info.SelectedRows.Count > 0 ? tabl_info.SelectedRows[0] : null;
            if (row2 == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var form = new PhDirecForm();
            {
                var getReq = "SELECT Id, surname, name, patronymic FROM" + "[" + AbonentTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    string setS = row["surname"].ToString() + " " + row["name"].ToString() + " " + row["patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }


                form.AbonentData = dict;
            }

            {
                var getReq = "SELECT Id, phone FROM" + "[" + ContactTableName + "]";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var dict = new Dictionary<int, string>();
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);

                foreach (DataRow row in Tbl.Rows)
                {
                    dict.Add((int)row["Id"], row["phone"].ToString());
                }


                form.ContactData = dict;
            }
            DataGridViewSelectedRowCollection Row = tabl_info.SelectedRows;

            var mas = tabl_info.SelectedRows;
            var Condidat = mas[0].Cells["Id"].FormattedValue.ToString();
            var Condidat2 = mas[0].Cells["Id1"].FormattedValue.ToString();

            var conn = new SqlConnection(ConnectionString);
            conn.Open();
            string str = "";
            var req = "SELECT surname FROM Borisyuk_abonent WHERE Id = " + Condidat + "";
            var com = new SqlCommand(req, conn);
            var last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT name FROM Borisyuk_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last + " ";
            req = "SELECT patronymic FROM Borisyuk_abonent WHERE Id = " + Condidat + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            str += last;
            form.FIO_PhDirecForm_ComBox.Text = str;

            req = "SELECT phone FROM Borisyuk_contact WHERE Id = " + Condidat2 + "";
            com = new SqlCommand(req, conn);
            last = com.ExecuteScalar();
            form.Phone_PhDirecForm_ComBox.Text = last.ToString();

            conn.Close();

            if (form.ShowDialog() == DialogResult.OK)
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var request = "UPDATE Borisyuk_abonent_has_contact "  + " SET contact_id='" + form.ContactID +
                    "', " + "abonent_id ='" + form.AbonentID + "' WHERE contact_id=" + Condidat2 +
                    " AND abonent_id=" + Condidat;
                com = new SqlCommand(request, conn);
                com.ExecuteNonQuery();

                conn.Close();


                updatePHONEDGW();
            }
        }

        private void DELETE_PH_DICTIONARY_Click(object sender, EventArgs e)
        {
           
            var row = tabl_info.SelectedRows.Count > 0 ? tabl_info.SelectedRows[0] : null;
            if (row == null)
            {
                MessageBox.Show("Выберите строку!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var num1 = row.Cells["Id"].Value.ToString();
            var num2 = row.Cells["Id1"].Value.ToString();
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            var request = "DELETE FROM Borisyuk_abonent_has_contact WHERE contact_id = '" + num2 + 
                "' AND abonent_id='" + num1 + "'";
            var command = new SqlCommand(request, connection);
            command.ExecuteNonQuery(); //выполняет комманду не требуя что-то взамен и ничего не возвращающий
            connection.Close();
            updatePHONEDGW();
        }
    } 
}
